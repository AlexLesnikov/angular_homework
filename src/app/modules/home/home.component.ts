import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  loginForm: FormGroup;

  ngOnInit(): void {
    this.createForm();
  }


  createForm() {
    this.loginForm = new FormGroup({
      login: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }


  onSubmit() {
    console.log(this.loginForm);
    if (this.loginForm.invalid) {
      return;
    }
    console.log(this.loginForm.value);

  }

}
