import { Component, OnInit, Input } from '@angular/core';
import { NewsService } from '../service/news.service';

@Component({
  selector: 'app-item-news',
  templateUrl: './item-news.component.html',
  styleUrls: ['./item-news.component.scss']
})
export class ItemNewsComponent implements OnInit {

  constructor(private newsService: NewsService) { }

  @Input() lalala;

  ngOnInit(): void {
  }

  clickOnTheItem(item) {
    this.newsService.setSelectedNews(item);
  }
}
