import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { NewsService } from '../service/news.service';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(private newsService: NewsService) { }

  restList = [];

  ngOnInit(): void {
    this.restList = this.newsService.getNews();
    // this.newsService.getData().subscribe(res => {
    //   this.restList = res;
    //   console.log(res);
    // });
  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');
  }

  ngOnDestroy(): void {
    console.log('ngOnDestroy');

  }


}
