import { Injectable } from '@angular/core';
import { News } from 'src/app/models/news';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }
  private listNews: News[] = [
    {id: 1, title: 'Lorem text lorem', description: 'some long text', author: 'Tom Cuk', date: new Date()},
    {id: 2, title: 'Lorem texttext', description: 'some lonlonglonglongg text', author: 'Tom Cuk1', date: new Date()},
    {id: 3, title: 'LoremLorem text', description: 'some long longlonglonglong', author: 'Tom Cuk2', date: new Date()},
    {id: 4, title: 'Lorem LoremLorem', description: 'somtexttexttexttexte long text', author: 'Tom Cuk3', date: new Date()},
  ];

  private selected: News;

  getSelectedNews(): News {
    return this.selected;
  }

  setSelectedNews(news: News) {
    this.selected = news;
  }

  getNews(): News[] {
    return this.listNews;
  }

  getNewsById(id: number): News {
    this.selected = this.listNews.find(item => item.id === id);
    return this.selected;
  }


  // getData(): Observable<News[]> {
  //   return this.http.get<News[]>('assets/news.json');
  // }

  getDataById(id): Observable<News> {
    return this.http.get<News>('assets/news.json').pipe(map((res: any) => {
       return res.find(item => item.id === id);
    }));
  }

}
